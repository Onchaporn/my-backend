const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/:message', function (req, res, next) {
  const { params } = req
  res.json({ message: 'Hello my name is Dream!!!', params })
})

module.exports = router
